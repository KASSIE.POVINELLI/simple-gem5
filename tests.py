import subprocess
import os
import sys

# defines
cwd = os.getcwd()
gem5_wd = "gem5-wd"
gem5_dev = "gem5-dev"
# read in command line arguments
full_system = False
if len(sys.argv) > 1:
    if sys.argv[1] == "--full-system":
        full_system = True
# TESTS
# perform a test-run of the simulator in syscall-emulation mode (hello world)
print("Performing test-run of gem5 simulator in se mode...")
# subprocess.run(["docker", "run", "--rm", "-v",
#                f"{cwd}/{gem5_wd}:/gem5", "-it", gem5_dev, "run-se"])
#run the above but output to a log file
subprocess.run(["docker", "run", "--rm", "-v",
                f"{cwd}/{gem5_wd}:/gem5", gem5_dev, "run-se"], stdout=open("test.log", "w"))
# perform a test-run of the simulator in full-system mode (hello world)
if full_system:
    print("Performing test-run of simulator in fs mode...")
    subprocess.run(["docker", "run", "--rm", "-v",
                   f"{cwd}/{gem5_wd}:/gem5", "-it", gem5_dev, "run-fs"])
